import { TranslateAppPage } from './app.po';

describe('translate-app App', () => {
  let page: TranslateAppPage;

  beforeEach(() => {
    page = new TranslateAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});

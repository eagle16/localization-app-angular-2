import { Component, OnInit } from '@angular/core';
import { AppComponent } from "../app.component"
import { Subject } from 'rxjs/Subject'
import { LanguageService } from "../translate/language.service";

@Component({
    selector: "about",
    templateUrl: "about.component.html",
    styleUrls: ["about.component.css"]
})
export class AboutComponent {
    lang: {};

    constructor(private languageService: LanguageService) { }

    static newLanguage = new Subject();

    ngOnInit() {
        this.languageService.getNewLang('en').subscribe((data) => this.lang = data); //default value - en
        AppComponent.onChangeLanguage.subscribe((data) => this.lang = data);
    }
}
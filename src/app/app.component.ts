import { Component, OnInit, DoCheck} from '@angular/core';

import { LanguageService } from "./translate/language.service";

import { HomeComponent } from './home/home.component';
import { Subject } from 'rxjs/Subject'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, DoCheck {

  title = 'app';

  lang: {};
  static onChangeLanguage = new Subject();

  constructor(private languageService: LanguageService) { }

  changeLang(choose) {
    let prefix = choose.getAttribute('data-lang');
    this.languageService.getNewLang(prefix).subscribe((data) => {
      this.lang = data;
      AppComponent.onChangeLanguage.next(this.lang);
    });
  }

  ngOnInit() {
    this.languageService.getNewLang('en').subscribe((data) => this.lang = data); //default value - en
    AppComponent.onChangeLanguage.next(this.lang);
  }

  ngDoCheck() {
    AppComponent.onChangeLanguage.next(this.lang);
  }
}

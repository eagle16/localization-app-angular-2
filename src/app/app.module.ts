import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule }   from '@angular/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { NotFoundComponent } from './not-found/not-found.component';

import { LanguageService } from "./translate/language.service";
import { routes } from './app.routes';

@NgModule({
  imports: [BrowserModule, RouterModule.forRoot(routes), HttpModule],
  declarations: [AppComponent, HomeComponent, AboutComponent, NotFoundComponent],
  providers: [LanguageService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { AppComponent } from "../app.component"
import { Subject } from 'rxjs/Subject'
import { LanguageService } from "../translate/language.service";

@Component({
    selector: 'home',
    templateUrl: "home.component.html",
    styleUrls: ["home.component.css"]
})
export class HomeComponent implements OnInit {

    lang:{};
    
    constructor(private languageService: LanguageService) { }

    static newLanguage = new Subject();

    ngOnInit() {
        this.languageService.getNewLang('en').subscribe((data) => this.lang = data); //default value - en
        AppComponent.onChangeLanguage.subscribe((data) => this.lang = data);
    }
}
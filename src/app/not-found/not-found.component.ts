import { Component, OnInit } from '@angular/core';
import { AppComponent } from "../app.component"
import { Subject } from 'rxjs/Subject'
import { LanguageService } from "../translate/language.service";

@Component({
    selector: "not-found",
    template: `<div><img src="{{lang?.notFound}}" alt="title"/></div>`,
    styles: ["div { text-align: center }"]

})
export class NotFoundComponent implements OnInit {
    lang: {};

    constructor(private languageService: LanguageService) { }

    static newLanguage = new Subject();

    ngOnInit() {
        this.languageService.getNewLang('en').subscribe((data) => this.lang = data); //default value - en
        AppComponent.onChangeLanguage.subscribe((data) => this.lang = data);
    }
}
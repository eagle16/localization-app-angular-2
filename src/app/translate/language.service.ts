import { Injectable } from "@angular/core";
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class LanguageService {
    constructor(private http: Http) { }

    getNewLang(prefix) {
        return this.http.get('/assets/' + prefix + '-lang.json')
            .map((data: Response) => {
                let wordsList = data.json();
                return wordsList
            });
    }
}
